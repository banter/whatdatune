var controllers = require('./controllers');

module.exports = function init(app) {
	app.get('/recordings', controllers.getRecordings);
	app.get('/recordings/byTag/:tag', controllers.getRecordingsByTag);
	app.get('/recording/:recordingId', controllers.getRecording);
	app.get('/recording/:recordingId/sound', controllers.getRecordingSound);
	app.get('/tags', controllers.getTags);
	app.put('/recording/', controllers.putRecording);
	app.put('/recording/:recordingId/comment', controllers.putComment);
	app.post('/recording/:recordingId/comment/:commentId/upvote', controllers.upvoteComment);
	app.post('/recording/:recordingId/comment/:commentId/downvote', controllers.downvoteComment);
	app.delete('/recording/:recordingId', controllers.deleteRecording);
};
