var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var mongourl = 'mongodb://localhost';

mongoose.connect(mongourl);

var tagSchema = mongoose.Schema({
	name: String
});

var Tag = mongoose.model('Tag', tagSchema);

var recordingSchema = mongoose.Schema({
	title: String,
	author: String,
	comments: [{
		author: String,
		body: String,
		timestamp: Date,
		votes: Number
	}],
	timestamp: Date,
	midi: [{
		note: String,
		timestamp: Number,
		duration: Number
	}],
	wavFile: String,
	tags: [String]
});

recordingSchema.plugin(mongoosePaginate);

var Recording = mongoose.model('Recording', recordingSchema);

exports.Tag = Tag;
exports.Recording = Recording;
