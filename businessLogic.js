var models = require('./models');

var ITEMS_PER_PAGE = 10;

function callCallback(callback, err, results) {
	if(err)
		callback(err);
	else
		callback(null, results);
}

function getTags(callback) {
	var result = {};
	models.Recording.find({}, {tags: 1}, function(err, recs) {
		for (var rec in recs) {
			var tags = recs[rec].tags;
			tags.forEach(function(tag) {
				if (!result[tag]) {
					result[tag] = true;
				}
			});
		}
		callback(null, Object.keys(result));
	});
}

function getRecording(recordingId, callback) {
	models.Recording.findById(recordingId, function(err, recording) {
		callCallback(callback, err, recording);
	});
}

function getRecordings(page, callback) {
	models.Recording.paginate({}, page, ITEMS_PER_PAGE, function(err, pageCount, paginatedResults, itemCount) {
		callCallback(callback, err, paginatedResults);
	}, { sortBy: { timestamp: -1 } });
}

function getRecordingsByTag(page, tag, callback) {
	models.Recording.paginate({ tags: tag }, page, ITEMS_PER_PAGE, function(err, pageCount, paginatedResults, itemCount) {
		callCallback(callback, err, paginatedResults);
	});
}

function putRecording(recording, recordedFile, callback) {
	if(recordedFile) {
		recording.midi = [];
		recording.wavFile = recordedFile;
	} else {
		recording.midi = JSON.parse(recording.midi);
		recording.wavFile = '';
	}

	var record = new models.Recording({
		author: recording.author,
		title: recording.title,
		timestamp: new Date(),
		tags: recording.tags,
		comments: [],
		midi: recording.midi,
		wavFile: recording.wavFile.replace("waves/", "")
	});

	record.save(function(err, recId) {
		callCallback(callback, err, {id: recId._id});
	});
}

function deleteRecording(recordingId, callback) {
	models.Recording.findByIdAndRemove(recordingId, function(err) {
		callCallback(callback, err);
	});
}

function putComment(recordingId, comment, callback) {
	models.Recording.findById(recordingId, function(err, recording) {
		var commentToPush = {
			author: comment.author,
			body: comment.body,
			timestamp: new Date(),
			votes: 0
		};

		recording.comments.push(commentToPush);

		recording.save(function(err) {
			callCallback(callback, err);
		});
	});
}

function upvoteComment(recordingId, commentId, callback) {
	models.Recording.findById(recordingId, function(err, recording) {
		recording.comments[commentId].votes++;

		recording.save(function(err) {
			callCallback(callback, err);
		});
	});
}

function downvoteComment(recordingId, commentId, callback) {
	models.Recording.findById(recordingId, function(err, recording) {
		recording.comments[commentId].votes--;

		recording.save(function(err) {
			callCallback(callback, err);
		});
	});
}

exports.getRecording = getRecording;
exports.getRecordings = getRecordings;
exports.getRecordingsByTag = getRecordingsByTag;
exports.putRecording = putRecording;
exports.deleteRecording = deleteRecording;
exports.putComment = putComment;
exports.upvoteComment = upvoteComment;
exports.downvoteComment = downvoteComment;
exports.getTags = getTags;
