var models = require('../models');

var tagMetal = new models.Tag({
	name: 'Metal'
});

tagMetal.save(function(err) {
	if(err) {
		console.log(err);
	}
});

var recordingWave = new models.Recording({
	author: 'Adrian',
	title: 'Nagranie',
	timestamp: new Date(),
	tags: ['Metal'],
	comments: [],
	midi: [],
	wavFile: 'adrianNagranie.wav'
});

recordingWave.save(function(err) {
	if(err) {
		console.log(err);
	}
});
