var businessLogic = require('../businessLogic');

function respond(result, item) {
	console.log('Printing ' + item);
	console.log(result);
	console.log('Ended printing ' + item);
}

var recording = {
	author: 'Adrianna',
	title: 'Czo',
	tags: 'Metal'
};

var recordingFile = 'costam.wav';

businessLogic.putRecording(recording, recordingFile, function(err, results) {
	respond(results, 'put recording result');
	businessLogic.getRecordings(1, function(err, results) {
		respond(results, 'recordings');
		var recordingId = results[0]._id;
		businessLogic.getRecordingsByTag(1, 'Metal', function(err, results) {
			respond(results, 'recordings by tag Metal');
			businessLogic.deleteRecording(recordingId, function(err, results) {
				businessLogic.getRecordings(1, function(err, results) {
					respond(results, 'recordings after deletion');
				});
			});
		});
	});
});


