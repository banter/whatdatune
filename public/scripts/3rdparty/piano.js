(function($) {
    //
    // Variables
    // --------------------------------------------------

	var recordingOnHtml = "RECORDING";
	var recordingOffHtml = "RECORD";

  
    // Lock event for play
    var lockEvent = {};

    // Timestamp marking the beginning of a recording
    var startRecordingTimestamp;

    // Timestamp of key down on the key being pressed
    var startNoteTimestamp;

    // The JSON describing the recorded key sequence
    var record = [];

	// True, if we should save the notes
	var isRecording = false;

    //
    // Events
    // --------------------------------------------------

    // Disable Select
    // --------------------------------------------------
    $('.piano').bind('selectstart dragstart', function(ev) {
      ev.preventDefault();
      return false;
    });

  // Piano Play Click
  // --------------------------------------------------
    $('.key > span').mousedown(function(){
		// Save note
		var me = $(this);
		var noteClick = me.attr('data-note');

        // Play sound
        notes[noteClick].play();

		if(isRecording) {
			startNoteTimestamp = new Date().getTime();
			if(!startRecordingTimestamp) {
				startRecordingTimestamp = startNoteTimestamp;
				startNoteTimestamp = 0;
			} else
				startNoteTimestamp -= startRecordingTimestamp;
		}
    });

    $('.key > span').mouseup(function(){
		if(isRecording) {
			// Save note
			var me = $(this);
			var noteClick = me.attr('data-note');
			
			var endNoteTimestamp = new Date().getTime() - startRecordingTimestamp;
			var noteDuration = endNoteTimestamp - startNoteTimestamp;
			var noteRecord = {
				'note': noteClick,
				'timestamp': startNoteTimestamp,
				'duration': noteDuration
			};
			record.push(noteRecord);
			
			// DEBUG
			console.log(record);
		}
    });

	$('#btnPianoRecord').click(function(){
		if(isRecording) {
			// set an invisible form field to record
			$('#midiPlayer').attr('midi', JSON.stringify(record));
			isRecording = false;
			$(this).html(recordingOffHtml);
		} else {
			// clear record and its field
			record = [];
			startRecordingTimestamp = undefined;
			$('#midiPlayer').attr('midi', '[]');
			isRecording = true;
			$(this).html(recordingOnHtml);
		}
	});
})(jQuery);
