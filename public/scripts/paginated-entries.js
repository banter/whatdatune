function paginatedEntries(selector, tag) {
    var url = tag ? "/recordings/byTag/" + tag : "/recordings";
    loadTemplate("templates/entry.html", function(tpl) {
        $(selector).pageless({
            currentPage: 0,
            url: url,
            loaderImage: "images/ajax-loader.gif",
            scrape: function (data) {
                data = JSON.parse(data);
				for(var i = 0; i < data.length; i++) {
					data[i].midi = JSON.stringify(data[i].midi);
				}
                return data.map(tpl).join("");
            }
        });
    });
}
