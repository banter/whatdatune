var templates = {};

if (!navigator.getUserMedia && !navigator.webkitGetUserMedia)
    routie({
        '*': function() { 
            loadTemplate("templates/unsupported.html", setTemplate.bind(null, {}));
        }
    });
else
    routie({
        '/': function() {
            loadTemplate("templates/master.html", setTemplate.bind(null, {}));
        },
        '/top': function() {
            loadTemplate("templates/bytag.html", setTemplate.bind(null, {
                title: 'Top recordings',
                tag: ''
            }));
        },
        '/tag/:tag': function(tag) {
            loadTemplate("templates/bytag.html", setTemplate.bind(null, {
                title: 'Recordings by tag ' + tag,
                tag: tag
            }));
        },
        '/recording/:id': function(id) {
            getRecording(id, function(rec) {
				rec.midi = JSON.stringify(rec.midi);
				rec.timestamp = (new Date(rec.timestamp)).toLocaleString();
				for(var i = 0; i < rec.comments.length; i++) {
					rec.comments[i].timestamp = (new Date(rec.comments[i].timestamp)).toLocaleString();
				}
                loadTemplate("templates/detail.html", setTemplate.bind(null, rec));
            });
        },
        '/recording/:id/addComment': function(id) {
            routie("/recording/" + id);
        },
        '*': function() {
            loadTemplate("templates/404.html", setTemplate.bind(null, {}));
        }
    });

routie('/');

function loadTemplate(url, callback) {
    if (!templates[url]) {
        $.ajax(url)
            .done(function(result) {
                templates[url] = Handlebars.compile(result);
                callback(templates[url]);
            });
    }
    else {
        callback(templates[url]);
    }
}

function setTemplate(obj, tpl) {
    $("#routie").html(tpl(obj || {}));
}
