function callbackErr(callback) {
    return function(err) { callback(err); };
}

function ajaxUploadFile(method, url, fileData, fileName, data, callback, errback) {
    var fd = new FormData();
    fd.append('fname', fileName);
    fd.append('wavFile', fileData);
    for (var key in data) {
        fd.append(key, data[key]);
    }
    $.ajax({
        type: method,
        url: url,
        data: fd,
        processData: false,
        contentType: false
    })
    .done(callback)
    .fail(errback);
}

function ajax(method, url, data, callback, errback) {
    return $.ajax({
        data: data,
        url: url,
        type: method
    })
    .done(callback)
    .fail(errback);
}

function getTags(callback, errback) {
    return ajax('GET', '/tags', null, callback, errback);
}

function getRecording(recordingId, callback, errback) {
    return ajax('GET', '/recording/' + recordingId, null, callback, errback);
}

function getRecordingsPage(pageId, tag, callback, errback) {
    var url = '/recordings' + tag ? '/byTag/' + tag : ''; 
    return ajax('GET', url, {page: pageId}, callback, errback);
}

function uploadRecording(recWav, author, title, tags, callback, errback) {
    return ajaxUploadFile('PUT', '/recording', recWav, 'file.wav', {
        author: author,
        title: title,
        tags: JSON.stringify(tags)
    }, callback, errback);
}

function uploadMidi(midi, author, title, tags, callback, errback) {
    return ajax('PUT', '/recording', {
        author: author,
        title: title,
		midi: midi,
        tags: JSON.stringify(tags)
    }, callback, errback);
}

function addComment(recordingId, author, body, callback, errback) {
    var url = '/recording/' + recordingId + '/comment'; 
    return ajax('PUT', url, {
        author: author,
        body: body
    }, callback, errback);
}

function voteComment(recordingId, commentId, vote, callback, errback) {
    var voteType = vote === 1 ? 'upvote' : 'downvote';
    var url = '/recording/' + recordingId + '/comment/' + commentId + '/' + voteType; 
	console.log(url);
    return ajax('POST', url, {}, callback, errback);
}
