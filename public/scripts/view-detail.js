function onSubmitCommentClicked(recordingId) {
    var author = $("#inputEmail").val();
    var body = $("#inputComment").val();
    $("#btnSubmitComment").prop("disabled", "disabled");
    addComment(recordingId, author, body, function onSuccess() { 
        routie("/recording/" + recordingId + "/addComment");
    });
}

function onUpvote(recordingId, commentId) {
    voteComment(recordingId, commentId, +1, function onSuccess() {
        routie("/recording/" + recordingId + "/addComment");  
    });
}

function onDownvote(recordingId, commentId) {
    voteComment(recordingId, commentId, -1, function onSuccess() {
        routie("/recording/" + recordingId + "/addComment");  
    });
}

$("#comments blockquote").sort(function(a, b) {
	return $(a).data('votes') < $(b).data('votes');
}).appendTo("#comments");
