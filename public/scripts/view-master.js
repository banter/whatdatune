$(document).ready(function() {
    $("#inputTags").tagsinput();
    paginatedEntries("#paginator");
});

function submitRecording() {
	var chosenTab = $("#tabs .active").attr("id");
	var tags = $("#inputTags").val().split(",");
	console.log(chosenTab);
	if(chosenTab == "tab_record") {
		uploadRecording(recordingBlob, 
			$("#inputEmail").val(),
			$("#inputDescription").val(),
			tags,
			function onSuccess(recMeta) {
				routie("/recording/" + recMeta.id);
			});
	} else {
		uploadMidi($('#midiPlayer').attr('midi'),
			$("#inputEmail").val(),
			$("#inputDescription").val(),
			tags,
			function onSuccess(recMeta) {
				routie("/recording/" + recMeta.id);
			});
	}
}
