var tagItemTemplate = Handlebars.compile(
    "<li><a href='#/tag/{{tag}}'>{{tag}}</a></li>"
);

$(document).ready(function() {
    getTags(function(tags) {
        $("#tagsList").html(tags.map(function(tag) { return tagItemTemplate({tag: tag}); }).join(""));
    });
});