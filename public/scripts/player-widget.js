var playerWidgets = {};

var playText = "<i class='fa fa-play'></i> PLAY";
var pauseText = "<i class='fa fa-pause'></i> PAUSE";
var resumeText = "<i class='fa fa-play'></i> RESUMé";

function widgetPlay() {
	console.log("widgetPlay");

	var id = $(this).parent().attr("id");
	$(this).html(pauseText);
	$(this).unbind().click(widgetPause);

	if($(this).parent().attr("wavFile")) {
		if(!playerWidgets[id]) {
			var wavFile = $(this).parent().attr("wavFile");
			playerWidgets[id] = new Audio(wavFile);
		}

		var button = $(this);

		playerWidgets[id].onended = function() {
			button.html(playText);
			button.unbind().click(widgetPlay);
		};
		playerWidgets[id].play();
	} else if($(this).parent().attr("midi")) {
		console.log($(this).parent().attr("midi"));
		var record = JSON.parse($(this).parent().attr("midi"));

		if(!playerWidgets[id])
			playerWidgets[id] = {};

		playerWidgets[id].record = record;
		playerWidgets[id].start = Number(new Date());
		playerWidgets[id].timeouts = [];

		for(var i = 0; i < record.length; i++) {
			var timeoutId = setTimeout("notes['" + record[i].note + "'].play();", record[i].timestamp);
			console.log("timeoutId: " + timeoutId);
			playerWidgets[id].timeouts.push(timeoutId);
		}

		var button = $(this);

		// end playing 500ms after last note
		var endTimeoutId = setTimeout(function() {
			button.html(playText);
			button.unbind().click(widgetPlay);
		}, record[record.length - 1].timestamp + 500);

		playerWidgets[id].timeouts.push(endTimeoutId);

	} else {
		console.log("not midi nor wav");
	}
}

function widgetPause() {
	console.log("widgetPause");

	$(this).html(resumeText);
	$(this).unbind().click(widgetResume);
	var id = $(this).parent().attr("id");

	if($(this).parent().attr("wavFile")) {
		playerWidgets[id].pause();
	} else if($(this).parent().attr("midi")) {
		var started = playerWidgets[id].start;
		playerWidgets[id].duration = Number(new Date()) - started;

		console.log(playerWidgets[id].timeouts);

		for(var i = 0; i < playerWidgets[id].timeouts.length; i++) {
			console.log("clearing timeout " + playerWidgets[id].timeouts[i]);
			clearTimeout(playerWidgets[id].timeouts[i]);
		}
		playerWidgets[id].timeouts = [];
	} else {
		console.log("not midi nor wav");
	}
}

function widgetResume() {
	console.log("widgetResume");

	$(this).html(pauseText);
	$(this).unbind().click(widgetPause);
	var id = $(this).parent().attr("id");

	if($(this).parent().attr("wavFile")) {
		playerWidgets[id].play();
	} else if($(this).parent().attr("midi")) {
		playerWidgets[id].start = new Date();
		var record = playerWidgets[id].record;

		for(var i = 0; i < record.length; i++) {
			var newTimestamp = record[i].timestamp - playerWidgets[id].duration;
			if(newTimestamp > 0)
				playerWidgets[id].timeouts.push(setTimeout("notes['" + record[i].note + "'].play();", newTimestamp));
		}
		
		var button = $(this);

		// end playing 500ms after last note
		var endTimeoutId = setTimeout(function() {
			button.html(playText);
			button.unbind().click(widgetPlay);
		}, record[record.length - 1].timestamp - playerWidgets[id].duration + 500);

		playerWidgets[id].timeouts.push(endTimeoutId);
	} else {
		console.log("not midi nor wav");
	}
}

$(".player").find("button").unbind().click(widgetPlay);
