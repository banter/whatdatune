var businessLogic = require('./businessLogic');

function getTags(req, res, next) {
	businessLogic.getTags(function(err, result) {
		res.send(result);
	});
}

function getRecordings(req, res, next) {
	var page = 1;
	if(req.query.page) {		
		page = req.query.page;
	}

	businessLogic.getRecordings(page, function(err, result) {
		res.send(result);
	});
}

function getRecordingsByTag(req, res, next) {
	var page = 1;
	if(req.query.page) {
		page = req.query.page;
	}

	var tag = req.params.tag;

	businessLogic.getRecordingsByTag(page, tag, function(err, result) {
		res.send(result);
	});
}

function getRecording(req, res, next) {
	var recordingId = req.params.recordingId;

	businessLogic.getRecording(recordingId, function(err, result) {
		res.send(result); // TODO remove tags from result
	});
}

function getRecordingSound(req, res, next) {
	var recordingId = req.params.recordingId;

	businessLogic.getRecording(recordingId, function(err, result) {
		res.redirect(result.wavFile);
	});
}

function putRecording(req, res, next) {
	var recording = req.body;
	var recordedFile;
	if(req.files.wavFile)
		recordedFile = req.files.wavFile.path;
	recording.tags = JSON.parse(recording.tags);
	businessLogic.putRecording(recording, recordedFile, function(err, recId) {
		res.send(recId);
	});
}

function deleteRecording(req, res, next) {
	var recordingId = req.params.recordingId;

	businessLogic.deleteRecording(recordingId, function(err, result) {
		res.send(200);
	});
}

function putComment(req, res, next) {
	var recordingId = req.params.recordingId;
	var comment = req.body;

	businessLogic.putComment(recordingId, comment, function(err, result) {
		res.send(200);
	});
}

function upvoteComment(req, res, next) {
	var recordingId = req.params.recordingId;
	var commentId = req.params.commentId;
	console.log(recordingId);
	console.log(commentId);
	businessLogic.upvoteComment(recordingId, commentId, function(err, result) {
		console.log("ERROR: " + err);
		res.send(200); // TODO: change return code to a more appropriate one
	});
}

function downvoteComment(req, res, next) {
	var recordingId = req.params.recordingId;
	var commentId = req.params.commentId;

	businessLogic.downvoteComment(recordingId, commentId, function(err, result) {
		console.log("ERROR: " + err);
		res.send(200); // TODO: change return code to a more appropriate one
	});
}

exports.getRecordings = getRecordings;
exports.getRecordingsByTag = getRecordingsByTag;
exports.getRecording = getRecording;
exports.getRecordingSound = getRecordingSound;
exports.putRecording = putRecording;
exports.deleteRecording = deleteRecording;
exports.putComment = putComment;
exports.upvoteComment = upvoteComment;
exports.downvoteComment = downvoteComment;
exports.getTags = getTags;
